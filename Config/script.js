/* global system log Timer player*/

this.name = "RRSBlackBoxHC";
this.author = "SMax";
this.copyright = "2016 SMax";
this.licence = "CC-BY-NC-SA 4.0";
this.description = "Play the Hot&Cold game while looking the Black Box for RRS Groups";
this.version = "0.1";

"use strict";

this._DEBUG = false;

this._timer = null;
this._lastMSG = "";

this.shipLaunchedFromStation = function(station) {
	this._runGame();
};

this.shipDockedWithStation = function(station) {
	this._timerStop();
};

this.shipWillExitWitchspace = function() {
	this._runGame();
};

this._logger = function(msg) {
	if (this._DEBUG) {
		log(this.name, msg);
	}
};

this._timerStop = function() {
	if (this._timer) {
		this._timer.stop();
		this._timer = null;
		this._lastMSG = "";
	}
}

this._getBB = function() {
	var bb = system.shipsWithPrimaryRole("rescue_blackbox");

	if (bb && bb[0]) {
		return bb[0];
	}

	this._timerStop();
	return null;
};

this._runGame = function() {
	var bb = this._getBB();
	if (!bb) {
		return;
	}

	player.consoleMessage("Hot&Cold game started! Find the Black Box!");
	this._timer = new Timer(this, this._timerCallBack, 1, 1);
};

this._timerCallBack = function() {
	var bb = this._getBB();
	if (!bb) {
		return;
	}

	var vecBB = bb.position.subtract(player.ship.position);
	var alpha = vecBB.angleTo(player.ship.orientation.vectorForward());
	var msg = "";

	if (alpha < (Math.PI / 4)) {
		msg = "Very hot";
	}
	else if (alpha < (Math.PI / 2)) {
		msg = "Hot";
	}
	else {
		msg = "Cold";
	}

	if (this._lastMSG != msg) {
		player.consoleMessage("Black Box - " + msg + "!");
		this._lastMSG = msg;
	}
};