RRS Black Box - Hot&Cold game

By S Max

Play the Hot&Cold game while looking the Black Box for RRS Groups.

Available in [Oolite](http://www.oolite.org/) package manager (Activities).

# License
This work is licensed under the Creative Commons Attribution-Noncommercial-Share Alike 4.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/

# Version History
## [CURRENT] - 2016-09-27

- Fix OXP category

## [0.1] - 2016-09-24

- Initial release

